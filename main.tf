# resource "civo_instance" "k3d" {
#     hostname = "bigbang.dev"
#     tags = ["python", "nginx"]
#     notes = "this is the k3d server"
#     size = "g3.large"
#     disk_image = "ubuntu-jammy"
# }

# Query large instance size
# data "civo_size" "large" {
#     filter {
#         key = "type"
#         values = ["kubernetes"]
#     }

#     filter {
#         key = "name"
#         values = ["g3.large"]
#     }
# }

# Create a network
resource "civo_network" "cluster_net" {
  label = "cluster-network"
  region = "NYC1"
}

# Create a firewall
resource "civo_firewall" "cluster-firewall" {
    name = "cluster-firewall"
    network_id = civo_network.cluster_net.id
    region = "NYC1"
    create_default_rules = false
    ingress_rule {
        label      = "k8s"
        protocol   = "tcp"
        port_range = "6443"
        cidr       = [var.local_cidr, "192.168.10.4/32", "192.168.10.10/32"]
        action     = "allow"
    }
    
    ingress_rule {
        label      = "ssh"
        protocol   = "tcp"
        port_range = "22"
        cidr       = [var.local_cidr, "192.168.10.4/32", "192.168.10.10/32"]
        action     = "allow"
    }

    egress_rule {
        label      = "all"
        protocol   = "tcp"
        port_range = "1-65535"
        cidr       = ["0.0.0.0/0"]
        action     = "allow"
    }
}

# Create a cluster with k3s
resource "civo_kubernetes_cluster" "big-bang-dev" {
    name = "big-bang-dev"
    applications = "flux,-traefik2-nodeport"
    firewall_id = civo_firewall.cluster-firewall.id
    network_id = civo_network.cluster_net.id
    region = "NYC1"
    cluster_type = "k3s"
    kubernetes_version = "1.27.1-k3s1"
    pools {
        label = "front-end" // Optional
        size = "g4s.kube.large"
        node_count = 1
    }
}