helm upgrade --install bigbang $HOME/repos/bigbang/chart \
  --values https://repo1.dso.mil/platform-one/big-bang/bigbang/-/raw/master/chart/ingress-certs.yaml \
  --values ib_creds.yaml \
  --values demo_values.yaml \
  --namespace=bigbang --create-namespace
